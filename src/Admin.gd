extends KinematicBody2D

signal helping_user
signal helped_user

export var speed = 200

onready var Area = $Area
onready var Anim = $AnimationPlayer
onready var Camera = $Camera
onready var Game = get_node('/root/Game')
onready var VIEW_SIZE = get_viewport().size
onready var half_view = Vector2(VIEW_SIZE.x / 2, VIEW_SIZE.y / 2)

var helping = false
var in_help_zone = null

const GAMES = [
	preload('res://minigames/PopupMadness.tscn')
]

func _ready():
	add_to_group('admin')
	Area.add_to_group('admin_area')
	Area.connect('area_entered', self, 'area_entered')

func _physics_process(delta):
	var help = false

	# input processing
	var velocity = Vector2()
	if !helping:
		if Input.is_action_pressed("go_up"):
			velocity.y -= 1
		if Input.is_action_pressed("go_down"):
			velocity.y += 1
		if Input.is_action_pressed("go_left"):
			velocity.x -= 1
		if Input.is_action_pressed("go_right"):
			velocity.x += 1
		if Input.is_action_just_released("ui_accept"):
			help = true

	if help and in_help_zone and in_help_zone.need_help:
		play_minigame(in_help_zone)

	if velocity.length() > 0:
		velocity = velocity.normalized() * speed
		walk_anim(velocity)
		# warning-ignore:return_value_discarded
		move_and_collide(velocity * delta)
	else:
		Anim.stop()
	update_z(position.y)

func update_z(y):
	if y < 250: z_index = 0
	elif y < 660: z_index = 1
	elif y < 900: z_index = 2
	else: z_index = 3

func walk_anim(v):
	if v.x > 0: Anim.play('right')
	elif v.x < 0: Anim.play('left')
	elif v.y > 0: Anim.play('down')
	elif v.y < 0: Anim.play('up')

func area_entered(area):
	if area.is_in_group('users'):
		in_help_zone = area.get_parent()

func area_exited(area):
	if area.is_in_group('users'):
		in_help_zone = null

func play_minigame(user):
	helping = true

	emit_signal('helping_user', user)
	var minigame = GAMES[0].instance()
	Camera.add_child(minigame)
	# minigame.position += half_view
	var mg_score = yield(minigame, 'finished')
	Camera.remove_child(minigame)
	emit_signal('helped_user', user)

	print('scored %s' % mg_score)
	helping = false
