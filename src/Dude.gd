extends StaticBody2D

export var Seed = 10

const BLINK = 'blink'
var need_help = false
onready var Area = $Area
onready var Anim = $AnimationPlayer

func _ready():
	Area.add_to_group('users')
	Anim.play('idle')

func catch_fire():
	need_help = true
	Anim.play('panic')

func chill():
	need_help = false
	Anim.play('idle')
