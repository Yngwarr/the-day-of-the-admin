extends Node2D

signal window_clicked

export(Texture) var texture = load('res://img/popup/pop1.png')

onready var Window = $Window

var dragging = false

func _ready():
	# warning-ignore:return_value_discarded
	get_node("Window/Close").connect('button_up', self, 'close')
	Window.connect('gui_input', self, 'drag')
	set_texture(texture)

func drag(e):
	if e is InputEventMouseButton and e.button_index == BUTTON_LEFT:
		dragging = e.pressed
	if dragging and e is InputEventMouseMotion:
		Window.rect_position += e.relative

func set_texture(t):
	Window.texture = t

func move(pos):
	position = pos

func close():
	get_parent().remove_child(self)
