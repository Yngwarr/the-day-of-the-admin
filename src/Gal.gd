extends StaticBody2D

export var Seed = 10

const BLINK = 'blink'
var need_help = false
onready var Area = $Area
onready var Anim = $AnimationPlayer
onready var Timer = $Timer
onready var rng = RandomNumberGenerator.new()

func _ready():
	rng.seed = Seed
	Area.add_to_group('users')
	Timer.connect('timeout', self, 'reload_blink')
	Timer.start()

func catch_fire():
	need_help = true
	Timer.stop()
	Anim.play('panic')

func chill():
	need_help = false
	reload_blink()

func reload_blink():
	Anim.play(BLINK)
	var dt = rng.randf_range(0.5, 3.0)
	Timer.wait_time = dt
	Timer.start()
