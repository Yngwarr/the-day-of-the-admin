extends Node2D

var cursor = preload('res://img/cursor.png')

func _ready():
	Input.set_custom_mouse_cursor(cursor)
	$Admin.connect('helping_user', $HUD, 'fade')
	$Admin.connect('helped_user', $HUD, 'unfade')
	$Orchestrator.connect('update_fire', $HUD, 'update_fire')
