extends Node

signal update_fire

onready var Users = get_node('/root/Game/Users')
onready var Admin = get_node('/root/Game/Admin')
onready var rng = RandomNumberGenerator.new()
onready var Timer = $Timer

var chilling = []
var on_fire = 0

func _ready():
	rng.randomize()
	Admin.connect('helped_user', self, 'chill')
	Admin.connect('helping_user', self, 'pause_timer')
	chilling = Users.get_children()
	Timer.connect('timeout', self, 'step')
	Timer.start()

func step():
	var ignite = rng.randi_range(0, len(chilling) - 1)
	if len(chilling) != 0:
		chilling[ignite].catch_fire()
		chilling.remove(ignite)
		on_fire += 1
		emit_signal('update_fire', on_fire)
	Timer.wait_time = rng.randi_range(3.0, 6.0) if on_fire < 5 else rng.randi_range(5.0, 7.0)
	Timer.start()

func pause_timer(_user):
	Timer.paused = true

func chill(user):
	user.chill()
	chilling.push_back(user)
	on_fire -= 1
	emit_signal('update_fire', on_fire)
	Timer.paused = false
