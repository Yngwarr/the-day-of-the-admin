extends CanvasLayer

onready var Fade = $Fade
onready var OnFire = $OnFire

func _ready():
	unfade(null)

func fade(_user):
	Fade.visible = true

func unfade(_user):
	Fade.visible = false

func update_fire(n):
	OnFire.text = '%s' % n
	if n == 7:
		OnFire.material.set_shader_param('red', true)
	else:
		OnFire.material.set_shader_param('red', false)
