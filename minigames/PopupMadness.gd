extends CanvasLayer

# emitted with score; score non-positive for losing
signal finished

export var start_windows = 7

const TEXTURES = [
	preload('res://img/popup/pop1.png'),
	preload('res://img/popup/pop2.png'),
	preload('res://img/popup/pop3.png'),
	preload('res://img/popup/pop4.png'),
	preload('res://img/popup/pop5.png'),
	preload('res://img/popup/pop6.png')
]

var Window = preload('res://Window.tscn')
var rng = RandomNumberGenerator.new()
onready var Desktop = $Desktop
onready var Button = get_node("Desktop/Button")
onready var desktop_size = Desktop.rect_size

func _ready():
	rng.randomize()
	desktop_size.y -= get_node('Desktop/Taskbar').rect_size.y
	Button.connect('pressed', self, 'win')
	Button.rect_position += Vector2(rng.randi_range(-100, 100), rng.randi_range(-100, 100))
	TEXTURES.shuffle()
	for t in TEXTURES:
		spawn_window(t)

func win():
	emit_signal('finished', 8)

func spawn_window(texture):
	# TODO add sound
	var size = Vector2(
		rng.randi_range(100, desktop_size.x / 2),
		rng.randi_range(100, desktop_size.y / 2))
	var half_size = Vector2(size.x / 2, size.y / 2)
	var pos = Vector2(
		rng.randi_range(half_size.x, desktop_size.x - half_size.x),
		rng.randi_range(half_size.y, desktop_size.y - half_size.y))
	var win = Window.instance()

	Desktop.add_child(win)
	win.move(pos)
	win.set_texture(texture)
	win.add_to_group('windows')
